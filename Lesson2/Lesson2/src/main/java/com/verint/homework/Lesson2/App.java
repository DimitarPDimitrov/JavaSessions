package com.verint.homework.Lesson2;
import java.util.ArrayList;
import java.util.List;

import com.verint.fishes.*;
import com.verint.aquarium.*;

public class App 
{
    public static void main( String[] args )
    {
    	Fish paul = new Gupa("Paul", 1, "Sallty", "Yes");
    	Fish ringo = new Heler("Ringo", 2, "Clear", "No");
    	Fish george = new Oscar("Jeorge", 1, "Sallty", "Yes");
    	Fish john = new Gupa("John", 1, "Clear", "No");
    	
    	List<Fish> fishes = new ArrayList<>();
    	fishes.add(paul);
    	fishes.add(ringo);
    	fishes.add(george);
    	fishes.add(john);
    	
    	Acuarium Verint = new Acuarium();
    	Verint.checkFishes(fishes); 	
    }
}
