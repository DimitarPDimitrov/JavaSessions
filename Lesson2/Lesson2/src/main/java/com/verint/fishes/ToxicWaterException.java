package com.verint.fishes;

public class ToxicWaterException extends Exception {
	
	public ToxicWaterException(String message) {
		super(message);
	}
}