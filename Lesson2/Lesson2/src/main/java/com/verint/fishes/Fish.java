package com.verint.fishes;

public abstract class Fish {

	protected String name;
	private int speed;
	private String waterType;
	public String isWaterClear;
		
	public Fish(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public String getWaterType() {
		return waterType;
	}

	public void setWaterType(String waterType) {
		this.waterType = waterType;
	}
	
	public String getIsWaterClear() {
		return isWaterClear;
	}

	public void setIsWaterClear(String isWaterClear) {
		this.isWaterClear = isWaterClear;
	}

	public abstract int swim(int speed);
	
	public abstract String dive(String waterIsClear) throws ToxicWaterException;
	
}
