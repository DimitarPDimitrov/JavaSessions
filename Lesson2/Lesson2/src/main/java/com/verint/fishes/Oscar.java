package com.verint.fishes;

public class Oscar extends Fish {

	public Oscar(String name, int speed, String waterType, String isWaterClear) {
		super(name);
		this.setSpeed(speed);
		this.setWaterType(waterType);
		this.setIsWaterClear(isWaterClear);
	}
	@Override
	public int swim(int speed) {
		System.out.println(this.getName() + " is a " + getClass().getSimpleName() + ". It swims with " + this.getSpeed() + " km per hour.");
		return speed;
	}

	@Override
	public String dive(String isWaterClear) throws ToxicWaterException{
		if(isWaterClear == this.getIsWaterClear()) {
			System.out.println(this.getName() + " dives in " + this.getWaterType() + " water.");
			return this.getWaterType();
		}
		else throw new ToxicWaterException("Water is not clear. " + getClass().getSimpleName() + " can swim only in clear water.");
	}
}
