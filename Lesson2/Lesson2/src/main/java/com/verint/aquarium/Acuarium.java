package com.verint.aquarium;

import java.util.List;
import com.verint.fishes.*;

public class Acuarium {
	
	public void checkFishes(List<Fish> fishes) {
		
		for (Fish tempFish: fishes) {
			System.out.println("*-----------------------StartCheckFish " + tempFish.getName()+ " ----------------------*");
			tempFish.swim(tempFish.getSpeed());
			try {
				tempFish.dive("Yes");
			} catch (ToxicWaterException e) {
				System.out.println(e);
			} finally {
				System.out.println("*------------------------EndCheck-----------------------------*" + "\r\n");
			}
		}
	}
}