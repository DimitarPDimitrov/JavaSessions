package generics;

import java.util.Comparator;

public class Pair<K, V extends Comparable<V>> implements Comparable<Pair<K, V>>{
	
	private K key;
	private V value;
	
	public Pair(K key, V value){
		this.key = key;
		this.value = value;
	}

	public K getKey() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}
	
	public final Comparator<Pair> keyComperator = new Comparator<Pair>() {
		
		public int compare(Pair p1, Pair p2) {
			return p1.getValue().compareTo(p2.getValue());
		}
	};
	
	@Override
	public int compareTo(Pair<K, V> other) {
	    return this.getValue().compareTo(other.getValue());
	}

	@Override
	public String toString() {
		return "Pair [key = " + key + ", value = " + value + "]";
	}
}