package generics;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class main {
	public static void main(String [] args) {
		
		Pair[] pairs = new Pair[4];
		
		pairs[0] = new Pair<String, Integer>("key", 40);
		pairs[1] = new Pair<String, Integer>("key", 41);
		pairs[2] = new Pair<String, Integer>("key1", 40);
		pairs[3] = new Pair<String, Integer>("key", 43);
		
		Arrays.sort(pairs);
		
		for (Pair pair : pairs) {
			System.out.println(pair.toString());
		}
		System.out.println("");
		
		HashSet<Pair> nameScore = new HashSet();
		Pair ivan = new Pair<String, Integer>("Ivan", 33);
		Pair viktor = new Pair<String, Integer>("Victor", 31);
		Pair pepi = new Pair<String, Integer>("Petar", 30);
		nameScore.add(ivan);
		nameScore.add(viktor);
		nameScore.add(pepi);
		
		Iterator iterator = nameScore.iterator();
		while(iterator.hasNext()){
			System.out.println("Name: " + iterator.next().toString());
		}	
	}
}
