package junit.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ShoppingCartStubTest {

    @Test
    public void testAddOrder() {
    	DatabaseAccessor database = new DatabaseAccessorStub();
    	ShoppingCart shoppingCart = new ShoppingCart("Гошо", database);
    	assertTrue(shoppingCart.addOrder("Boza"));
    }
    
    @Test
    public void testSendOrders() {
    	DatabaseAccessor database = new DatabaseAccessorStub();
    	ShoppingCart shoppingCart = new ShoppingCart("Гошо", database);
    	assertFalse(shoppingCart.sendOrders());
    }
}