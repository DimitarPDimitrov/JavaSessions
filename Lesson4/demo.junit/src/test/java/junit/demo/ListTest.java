package junit.demo;

import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ListTest {
	
	@Test
	public void testSize() {
		List mockedList = mock(List.class);
		when(mockedList.size()).thenReturn(10).thenReturn(20);
		assertEquals(10, mockedList.size());
		assertEquals(20, mockedList.size());
	}
	
	
	@Test
	public void testGet() {
		List mockedList = mock(List.class);
		when(mockedList.get(Mockito.anyInt())).thenReturn(10);
		assertEquals(10, mockedList.get(42));
	}

}
