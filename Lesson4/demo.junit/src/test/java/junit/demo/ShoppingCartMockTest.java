package junit.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ShoppingCartMockTest {
	
	@Mock
	DatabaseAccessor databaseMock;
	
	@InjectMocks
	ShoppingCart shoppingCart;

    @Test
    public void testAddOrder() {
    	DatabaseAccessor databaseMock = mock(DatabaseAccessor.class);
    	ShoppingCart shoppingCart = new ShoppingCart("Гошо", databaseMock);
    	assertTrue(shoppingCart.addOrder("Boza"));
    }
    
    @Test
    public void testSendOrders() {
    	DatabaseAccessor databaseMock = mock(DatabaseAccessor.class);
    	when(databaseMock.saveOrder("Гошо", null)).thenReturn(true);
    	ShoppingCart shoppingCart = new ShoppingCart("Гошо", databaseMock);
//    	shoppingCart.setUserId("Гошо");
    	shoppingCart.setOrders(null);
    	assertTrue(shoppingCart.sendOrders());
    }
}