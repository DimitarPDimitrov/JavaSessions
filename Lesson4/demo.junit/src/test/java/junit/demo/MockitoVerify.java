package junit.demo;

import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

import java.util.List;

public class MockitoVerify {
	
	@Test
	public void testVerify()  {
		
	    // create and configure mock
	    List<Integer> test = Mockito.mock(List.class);
	    when(test.size()).thenReturn(42);


	    // call method add on the mock with parameter 12
	    test.add(12);


	    // now check if method add was called with the parameter 12
	    verify(test).add(ArgumentMatchers.eq(12));

	    // was the method called twice?
	    verify(test, times(2)).add(12);
	}
}
