package junit.demo;

import java.util.List;

public interface DatabaseAccessor {
	
	public boolean saveOrder(String userId, List<String> orderIds);
	
	public List<Integer> getClientOrderPrices(String clientId);
	
	public boolean isClientBlocked(String clientId);
	
	public String getClientRegistrationDate(String clientId);

}
