package junit.demo;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
	
	private String userId;
	private DatabaseAccessor dbAccess;
	private List<String> orders;

	public ShoppingCart(String userId, DatabaseAccessor dbAccess) {
		super();
		this.userId = userId;
		this.dbAccess = dbAccess;
		this.orders = new ArrayList<String>();
	}
	
	public boolean addOrder(String orderId) {
		return this.orders.add(orderId);
	}
	
	public boolean sendOrders() {
		return dbAccess.saveOrder(this.userId, orders);
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<String> getOrders() {
		return orders;
	}

	public void setOrders(List<String> orders) {
		this.orders = orders;
	}

}
