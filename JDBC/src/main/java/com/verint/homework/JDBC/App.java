package com.verint.homework.JDBC;
import java.sql.*;

public class App 
{
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost/democustomers";
	   
	   static final String USER = "DDimitrov";
	   static final String PASS = "password123456";
	   
    public static void main( String[] args )
    {
    	Connection conn = null;
    	Statement stmt = null;
    	
    	selectAllEmploeeys(conn, stmt);
    	
    	
    	try {
    		Class.forName("com.mysql.cj.jdbc.Driver");
    	    conn = DriverManager.getConnection(DB_URL,USER,PASS);
    	    

    	    String sql;
    	    sql = " insert into Emploees (firstName, lastName, age, company)"
    	            + " values (?, ?, ?, ?)";
    	    stmt = conn.prepareStatement(sql);
    	      PreparedStatement preparedStmt = conn.prepareStatement(sql);
    	      preparedStmt.setString (1, "John");
    	      preparedStmt.setString (2, "Johnson");
    	      preparedStmt.setInt(3, 25); 
    	      preparedStmt.setString(4, "Foxcon");

    	      preparedStmt.execute();
    	      
    	      preparedStmt.close();
    	      stmt.close();
    	      conn.close();
	    }catch(SQLException se){
	        se.printStackTrace();
	     }catch(Exception e){
	        e.printStackTrace();
	     }finally{
	        try{
	           if(stmt!=null)
	              stmt.close();
	        }catch(SQLException se2){
	        }
	        try{
	           if(conn!=null)
	              conn.close();
	        }catch(SQLException se){
	           se.printStackTrace();
	      }
	   }
    	selectAllEmploeeys(conn, stmt);
    	
    }

	private static void selectAllEmploeeys(Connection conn, Statement stmt) {
		try {
    		Class.forName("com.mysql.cj.jdbc.Driver");
    	    conn = DriverManager.getConnection(DB_URL,USER,PASS);
    	    stmt = conn.createStatement();

    	    String sql;
    	    sql = "SELECT id, firstName, lastName, age, company FROM Employees";
    	    ResultSet rs = stmt.executeQuery(sql);
    	    
	      while(rs.next()){
	          int id  = rs.getInt("id");
	          String first = rs.getString("firstName");
	          String last = rs.getString("lastName");
	          int age = rs.getInt("age");
	          String company = rs.getString("company");

	          System.out.print("ID: " + id);
	          System.out.print(", First Name: " + first);
	          System.out.print(", Last Name: " + last);
	          System.out.print(", Age: " + age);
	          System.out.print(", Company: " + company);
	          System.out.println("");
	       }
	      rs.close();
          stmt.close();
          conn.close();
	    }catch(SQLException se){
	        se.printStackTrace();
	     }catch(Exception e){
	        e.printStackTrace();
	     }finally{
	        try{
	           if(stmt!=null)
	              stmt.close();
	        }catch(SQLException se2){
	        }
	        try{
	           if(conn!=null)
	              conn.close();
	        }catch(SQLException se){
	           se.printStackTrace();
	      }
	   }
	}
}
